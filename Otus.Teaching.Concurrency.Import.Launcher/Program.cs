﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Enums;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Core.ValidateArgs;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Loader;
using Otus.Teaching.Concurrency.Import.Loader.Factories;

namespace Otus.Teaching.Concurrency.Import.Launcher
{
    class Program
    {
        private static Launchers _launcher = Launchers.Method;
        private static string _dataFilePath = string.Empty;
        private static int _threadNumber = 5;
        private static int _tryCount = 3;
        private static DataGeneratorTypes _dataGeneratorType = DataGeneratorTypes.Xml;
        private static IConfiguration _configuration;
        static void Main(string[] args)
        {
            args = new string[] { "file", "Csv", "1000", "Method", "5", "3" };
            if (!TryValidateAndParseArgs(args))
                return;
            //генерация файла
            LauncherFactory launcherFactory = new LauncherFactory();
            launcherFactory.Create(_launcher).Run(args);

            ////парс файла
            IDataParser<List<Customer>> dataParser = ParserFactory.GetParser(_dataGeneratorType, _dataFilePath);
            List<Customer> data = dataParser.Parse();

            //загрузка файла
            Stopwatch stopWatch = new Stopwatch();

            Console.WriteLine($"Thread pool scheduler in thread: {Thread.CurrentThread.ManagedThreadId}...");
            Console.WriteLine("Handling queue...");
            stopWatch.Start();

            IDataLoader customerDataLoader = new CustomerDataLoader(data, _threadNumber, _tryCount, _configuration);
            customerDataLoader.LoadData();

            stopWatch.Stop();
            Console.WriteLine($"Handled queue in {stopWatch.Elapsed}...");
            Console.ReadKey();
        }
        private static bool TryValidateAndParseArgs(string[] args)
        {
            ValidateArgsManager validateArgsManager = new ValidateArgsManager()
                                                      .AddValidationArgs(new FileNameValidateArgs())
                                                      .AddValidationArgs(new DataTypeGeneratorValidateArgs())
                                                      .AddValidationArgs(new DataCountValidateArgs())
                                                      .AddValidationArgs(new LaunchMethodValidateArgs())
                                                      .AddValidationArgs(new ThreadNumberValidateArgs())
                                                      .AddValidationArgs(new TryCountValidateArgs());
            if (validateArgsManager.Validate(args))
            {
                _configuration = new ConfigurationBuilder()
                                .AddJsonFile("appsettings.json")
                                .Build();
                ParseArgs(args);
                return true;
            }
            return false;
        }
        private static void ParseArgs(string[] args)
        {
            switch (args.Length)
            {
                case 1:
                    _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"{args[0]}.{_dataGeneratorType}");
                    break;
                case 2:
                    _dataGeneratorType = (DataGeneratorTypes)Enum.Parse(typeof(DataGeneratorTypes), args[1]);
                    goto case 1;
                case 4:
                    _launcher = (Launchers)Enum.Parse(typeof(Launchers), args[3]);
                    goto case 2;
                case 5:
                    _threadNumber = int.Parse(args[4]);
                    goto case 4;
                case 6:
                    _tryCount = int.Parse(args[5]);
                    goto case 5;
            }
        }
    }
}
