﻿using Otus.Teaching.Concurrency.Import.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Launcher
{
    public interface ILauncher
    {
        Launchers Name { get; }
        void Run(string[] args);
    }
}
