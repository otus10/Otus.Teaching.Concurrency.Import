﻿using Otus.Teaching.Concurrency.Import.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Launcher
{
    public class MethodLauncher : ILauncher
    {
        public Launchers Name => Launchers.Method;

        public void Run(string[] args)
        {
            Import.XmlGenerator.Program.Main(args);
        }
    }
}
