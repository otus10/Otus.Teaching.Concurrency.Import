﻿using Otus.Teaching.Concurrency.Import.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Launcher
{
    public class LauncherFactory
    {
        public ILauncher Create(Launchers launcher)
        {
            return launcher switch
            {
                Launchers.Process => new ProcessLauncher(),
                Launchers.Method => new MethodLauncher(),
                _ => new MethodLauncher(),
            };
        }
    }
}
