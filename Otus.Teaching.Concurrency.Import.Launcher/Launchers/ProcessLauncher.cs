﻿using Otus.Teaching.Concurrency.Import.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Launcher
{
    public class ProcessLauncher : ILauncher
    {
        public Launchers Name => Launchers.Process;

        public void Run(string[] args)
        {

            string assemblyName= typeof(Import.XmlGenerator.Program).Assembly.ManifestModule.Name;
            string exeName = System.IO.Path.GetFileNameWithoutExtension(assemblyName) + ".exe";
            ProcessStartInfo processStartInfo = new ProcessStartInfo()
            {
                FileName = exeName,
                Arguments = string.Join(" ", args.Take(2)),
            };
            Process.Start(processStartInfo);
        }
    }
}
