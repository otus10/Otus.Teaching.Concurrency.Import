﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using CsvHelper;
using CsvHelper.Configuration;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser
        : IDataParser<List<Customer>>
    {
        private readonly string _filePath;
        public CsvParser(string filePath)
        {
            this._filePath = filePath;
        }
        public List<Customer> Parse()
        {
            List<Customer> customers = new List<Customer>();
            using (StreamReader reader = new StreamReader(_filePath))
            using (CsvReader csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                customers.AddRange(csv.GetRecords<Customer>());
            }
            return customers;
        }
    }
}