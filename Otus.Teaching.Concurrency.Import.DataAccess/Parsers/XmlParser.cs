﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        private readonly string _filePath;
        public XmlParser(string filePath)
        {
            this._filePath = filePath;
        }
        public List<Customer> Parse()
        {
            using Stream stream = File.OpenRead(_filePath);
            XmlSerializer _serializer = new XmlSerializer(typeof(CustomersList));
            return ((CustomersList)_serializer.Deserialize(stream)).Customers;
        }
    }
}