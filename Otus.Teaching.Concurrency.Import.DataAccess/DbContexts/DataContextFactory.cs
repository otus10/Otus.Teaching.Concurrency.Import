﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using static System.Net.Mime.MediaTypeNames;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class DataContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext (string connectionString)
        {
            DbContextOptions<DataContext> options = new DbContextOptionsBuilder<DataContext>()
                .UseNpgsql(connectionString
            , assembly => assembly.MigrationsAssembly(typeof(DataContext).Assembly.GetName().Name))
            .Options; ;

            return new DataContext(options);    
        }

        public DataContext CreateDbContext(string[] args)
        {
            return CreateDbContext(@"Server=localhost;Port=5432;Database=Customers; User Id=test;Password=test");
        }

    }
}
