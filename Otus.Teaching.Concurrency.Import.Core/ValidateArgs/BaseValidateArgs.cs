﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core.ValidateArgs
{
    public abstract class BaseValidateArgs : IValidateArgs
    {
        public abstract bool Validate(string[] args, int order);
        protected bool Validate(string[] args, int order, Func<string[], int, bool> func)
        {
            if (args != null && args.Length > order)
            {
                return func(args, order);
            }
            return true;
        }
    }
}
