﻿using Otus.Teaching.Concurrency.Import.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core.ValidateArgs
{
    public class LaunchMethodValidateArgs : BaseValidateArgs, IValidateArgs
    {
        public override bool Validate(string[] args, int order)
        {
            return base.Validate(args, order, Check);
        }
        public bool Check(string[] args, int order)
        {
            {
                if (!Enum.IsDefined(typeof(Launchers), args[order]))
                {
                    Console.WriteLine($"Launchers {args[order]}. Available launchers:{string.Join(", ", Enum.GetNames(typeof(Launchers)))}");
                    return false;
                }
                return true;
            }
        }
    }
}
