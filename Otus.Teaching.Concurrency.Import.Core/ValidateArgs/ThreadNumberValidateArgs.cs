﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core.ValidateArgs
{
    public class ThreadNumberValidateArgs : BaseValidateArgs, IValidateArgs
    {
        public override bool Validate(string[] args, int order)
        {
            return base.Validate(args, order, Check);
        }
        public bool Check(string[] args, int order)
        {
            if (!int.TryParse(args[order], out int threadNum))
            {
                Console.WriteLine($"Thread number {args[order]} must be integer");
                return false;
            }
            return true;
        }
    }
}
