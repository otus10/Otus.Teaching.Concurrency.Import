﻿using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core.ValidateArgs
{
    public class DataTypeGeneratorValidateArgs : BaseValidateArgs, IValidateArgs
    {
        public override bool Validate(string[] args, int order)
        {
            return base.Validate(args, order, Check);
        }
        private bool Check(string[] args, int order)
        {
            if (!Enum.IsDefined(typeof(DataGeneratorTypes), args[order]))
            {
                Console.WriteLine($"Data generator type {args[order]}.Available data generator types:{string.Join(", ", Enum.GetNames(typeof(DataGeneratorTypes)))}");
                return false;
            }
            return true;
        }
    }
}
