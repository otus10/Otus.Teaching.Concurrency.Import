﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core.ValidateArgs
{
    public class DataCountValidateArgs : BaseValidateArgs, IValidateArgs
    {
        public override bool Validate(string[] args, int order)
        {
            return base.Validate(args, order, Check);
        }
        private bool Check(string[] args, int order)
        {
            if (!int.TryParse(args[order], out int _dataCount))
            {
                Console.WriteLine($"Data count {args[order]} must be integer");
                return false;
            }
            return true;
        }
    }
}
