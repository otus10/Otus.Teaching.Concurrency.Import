﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core.ValidateArgs
{
    public interface IValidateArgs
    {
        bool Validate(string[] args, int order);
    }
}
