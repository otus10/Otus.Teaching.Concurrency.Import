﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core.ValidateArgs
{
    public class FileNameValidateArgs : BaseValidateArgs, IValidateArgs
    {
        public override bool Validate(string[] args, int order)
        {
            return base.Validate(args, order, Check);
        }
        public bool Check(string[] args, int order)
        {
            if (!string.IsNullOrEmpty(Path.GetExtension(args[order])))
            {
                Console.WriteLine($"Data file name {args[order]} without extension is required");
                return false;
            }
            return true;
        }
    }
}
