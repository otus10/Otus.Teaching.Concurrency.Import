﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core.ValidateArgs
{
    public class TryCountValidateArgs : BaseValidateArgs, IValidateArgs
    {
        public override bool Validate(string[] args, int order)
        {
            return base.Validate(args, order, Check);
        }
        private bool Check(string[] args, int order)
        {
            if (!int.TryParse(args[order], out int tryCount))
            {
                Console.WriteLine($"Try Count {args[order]} must be integer");
                return false;
            }
            return true;
        }
    }
}
