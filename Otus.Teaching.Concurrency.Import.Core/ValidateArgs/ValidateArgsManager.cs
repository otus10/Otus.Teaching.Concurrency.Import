﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core.ValidateArgs
{
    public class ValidateArgsManager: IValidateArgs
    {
        readonly List<IValidateArgs> _validateArgs = new List<IValidateArgs> ();

        public ValidateArgsManager AddValidationArgs(IValidateArgs validateArgs)
        {
            _validateArgs.Add(validateArgs);
            return this;
        }

        public bool Validate(string[] args, int order = 0)
        {
            foreach (IValidateArgs validateArgs in _validateArgs)
            {
                if (!validateArgs.Validate(args, _validateArgs.IndexOf(validateArgs)))
                    return false;
            }
            return true;
        }
    }
}
