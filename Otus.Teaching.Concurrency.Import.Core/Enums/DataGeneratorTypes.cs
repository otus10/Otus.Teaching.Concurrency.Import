﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Core.Enums
{
    public enum DataGeneratorTypes
    {
        Xml,
        Csv
    }
}
