﻿using Microsoft.Extensions.Configuration;
using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class CustomerDataLoader : IDataLoader
    {
        private readonly List<Customer> _data;
        private readonly int _threadNumber;
        private readonly int _retryCount;
        private readonly IConfiguration _configuration;

        public CustomerDataLoader(List<Customer> data, int threadNumber, int retryCount, IConfiguration configuration)
        {
            _data = data;
            this._threadNumber = threadNumber;
            this._retryCount = retryCount;
            _configuration = configuration;
        }
        public void LoadData()
        {
            ManualResetEvent[] doneEvents = new ManualResetEvent[_threadNumber];
            for (int i = 0; i < _threadNumber; i++)
            {
                doneEvents[i] = new ManualResetEvent(false);
                DataRangeId dataRangeId = CalculateDataBounds(i + 1, _threadNumber, _data);
                IDbContext dbContext = new DataContextFactory().CreateDbContext(_configuration.GetConnectionString("PostgreConnection"));
                CustomerThreadPoolItem item = new CustomerThreadPoolItem(_data, dataRangeId, _retryCount, dbContext, doneEvents[i]);
                ThreadPool.QueueUserWorkItem(item.ThreadPoolCallback, i);
            }
            WaitHandle.WaitAll(doneEvents);

        }

        private DataRangeId CalculateDataBounds(int partNum, int threadNumber, List<Customer> data)
        {
            int partSize = Convert.ToInt32(data.Count / Convert.ToDecimal(threadNumber));
            int lowerBound = (partNum - 1) * partSize;
            int upperBound = Math.Min(lowerBound + partSize, data.Count) - 1;
            return new DataRangeId(lowerBound, upperBound);
        }

    }
}


