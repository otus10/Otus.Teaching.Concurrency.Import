﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class DataRangeId
    {
        public int MinId { get; private set; }

        public int MaxId { get; private set; }

        public int CountElements => MaxId - MinId + 1; 

        public DataRangeId(int minId, int maxId)
        {
            MinId = minId;
            MaxId = maxId;
        }
        public override string ToString() => $"RangeId {MinId}:{MaxId}, Elements Count:{CountElements}";
    }
}
