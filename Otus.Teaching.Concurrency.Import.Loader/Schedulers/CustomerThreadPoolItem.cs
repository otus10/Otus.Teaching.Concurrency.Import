﻿using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public class CustomerThreadPoolItem
    {
        private readonly List<Customer> _data;
        private int _retryCount;
        private readonly IDbContext _dbContext;
        private readonly DataRangeId _rangeId;
        private ManualResetEvent _doneEvent { get; }
        public CustomerThreadPoolItem(List<Customer> data, DataRangeId rangeId, int retryCount, IDbContext dbContext, ManualResetEvent doneEvent)
        {
            _data = data;
            _rangeId = rangeId;
            _retryCount = retryCount;
            _dbContext = dbContext;
            _doneEvent = doneEvent;
        }

        public async Task ProcessAsync()
        {
            while (_retryCount > 0)
            {
                try
                {
                    await _dbContext.Customers.AddRangeAsync(_data.GetRange(_rangeId.MinId, _rangeId.CountElements));
                    await _dbContext.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    if (_retryCount >= 0)
                    {
                        Console.WriteLine($"Range {_rangeId} failed with error: {ex.InnerException.Message}. Trying one more time..");
                        this._retryCount--;
                    }
                    else
                        throw ex;
                }
            }
        }
        public async void ThreadPoolCallback(Object threadContext)
        {
            int threadIndex = (int)threadContext;
            Console.WriteLine($"Thread {threadIndex} started...");
            await ProcessAsync();
            Console.WriteLine($"Thread {threadIndex} result calculated...");
            _doneEvent.Set();
        }
    }
}
