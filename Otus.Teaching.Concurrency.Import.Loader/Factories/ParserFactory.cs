﻿using Otus.Teaching.Concurrency.Import.Core.Enums;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.Loader.Factories
{
    public static class ParserFactory
    {
        public static IDataParser<List<Customer>> GetParser(DataGeneratorTypes dataType, string fileName)
        {
            return dataType switch
            {
                DataGeneratorTypes.Xml => new XmlParser(fileName),
                DataGeneratorTypes.Csv => new CsvParser(fileName),
                _ => new XmlParser(fileName),
            };
        }
    }
}
