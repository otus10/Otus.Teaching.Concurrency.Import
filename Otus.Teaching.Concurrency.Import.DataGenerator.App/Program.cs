﻿using Otus.Teaching.Concurrency.Import.Core.Enums;
using Otus.Teaching.Concurrency.Import.Core.ValidateArgs;
using System;
using System.IO;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Otus.Teaching.Concurrency.Import.Launcher")]

namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    internal class Program
    {
        private static readonly string _dataFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
        private static string _dataFileName; 
        private static int _dataCount = 100;
        private static DataGeneratorTypes _dataGeneratorType = DataGeneratorTypes.Xml;

        internal static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args))
                return;
            
            var generator = GeneratorFactory.GetGenerator(_dataGeneratorType, _dataFileName, _dataCount);
            generator.Generate();

        }

        internal static bool TryValidateAndParseArgs(string[] args)
        {
            ValidateArgsManager validateArgsManager = new ValidateArgsManager()
                                          .AddValidationArgs(new FileNameValidateArgs())
                                          .AddValidationArgs(new DataTypeGeneratorValidateArgs())
                                          .AddValidationArgs(new DataCountValidateArgs());
            if (validateArgsManager.Validate(args))
            {
                ParseArgs(args);
                return true;   
            }
            return false;
        }
        private static void ParseArgs(string[] args)
        {
            switch (args.Length)
            {
                case 1:
                    _dataFileName = Path.Combine(_dataFileDirectory, $"{args[0]}.{_dataGeneratorType}");
                    break;
                case 2:
                    _dataGeneratorType = (DataGeneratorTypes)Enum.Parse(typeof(DataGeneratorTypes), args[1]);
                    goto case 1;
                case 3:
                    int.TryParse(args[2], out _dataCount);
                    goto case 2;
                default: 
                    goto case 3;
            }
        }
    }
}