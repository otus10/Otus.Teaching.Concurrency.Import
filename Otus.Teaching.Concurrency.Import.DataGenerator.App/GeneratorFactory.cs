using Otus.Teaching.Concurrency.Import.Core;
using Otus.Teaching.Concurrency.Import.Core.Enums;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using XmlDataGenerator = Otus.Teaching.Concurrency.Import.DataGenerator.Generators.XmlGenerator;
namespace Otus.Teaching.Concurrency.Import.XmlGenerator
{
    public static class GeneratorFactory
    {
        public static IDataGenerator GetGenerator(DataGeneratorTypes dataType, string fileName, int dataCount)
        {
            return dataType switch
            {
                DataGeneratorTypes.Xml => new XmlDataGenerator(fileName, dataCount),
                DataGeneratorTypes.Csv => new CsvGenerator(fileName, dataCount),
                _ => new XmlDataGenerator(fileName, dataCount),
            };
        }
    }
}